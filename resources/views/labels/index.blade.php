@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="card" style="background-color:#fff; border-color:#eee;">
          <div class="card-body">
            <h4 class="card-title">{{ $page_title }}</h4>
            <table class="table" id="labels-table">
                <thead>
                    <tr>
                        <th>Label Name</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($labels as $label)
                        <tr id="label-{{ $label->id }}" data-row-id="{{ $label->id }}">
                            <td class="row-col">
                                <span class="text-col">{{ $label->name }}</span>
                                <input type="text" style="display:none" name="name" class="input-col form-control" value="{{ $label->name }}">
                            </td>
                            <td class="action-col">
                                <button type="button" onclick="saveRow('#label-{{ $label->id }}','edit')" class="btn btn-outline-success btn-sm submit-btn" style="display:none;"><i class="fa fa-check"></i></button>
                                <button type="button" onclick="editRow('#label-{{ $label->id }}')" class="btn btn-outline-primary btn-sm edit-btn"><i class="fa fa-pencil"></i></button>
                                <button type="button" onclick="deleteRow('#label-{{ $label->id }}')" class="btn btn-outline-danger btn-sm delete-btn"><i class="fa fa-trash"></i></button>
                                <button type="button" onclick="cancelEditRow('#label-{{ $label->id }}')" class="btn btn-outline-danger btn-sm cancel-btn" style="display:none;"><i class="fa fa-times"></i></button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="2">
                            <button type="button" onclick="addData('#labels-table')" class="btn btn-outline-primary btn-sm col-lg-12"><i class="fa fa-plus-circle"></i> Add New</button>
                        </td>
                    </tr>
                </tfoot>
            </table>
          </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        var newRowCount = 0;
        function addData(tableid){
            var table = $(tableid);
            var tableBody = table.find('tbody');
            newRowCount++;
            tableBody.append(
                '<tr id="newRow-'+newRowCount+'">'+
                    '<td class="row-col">'+
                        '<input type="text" name="name" class="input-col form-control">'+
                    '</td>'+
                    '<td class="action-col">'+
                        '<button type="button" onclick="saveRow(\'#newRow-'+newRowCount+'\',\'insert\')" class="btn btn-outline-success btn-sm submit-btn"><i class="fa fa-check"></i></button> '+
                        '<button type="button" class="btn btn-outline-danger btn-sm" onclick="removeRow(\'#newRow-'+newRowCount+'\')" ><i class="fa fa-times"></i></button>'+
                    '</td>'+
                '</tr>'
            );
        }

        function deleteRow(rowid){
            c = confirm('Are you sure want to delete this?');
            if (!c) {
                return false;
            }
            var row = $(rowid);
            var dbrowid = row.attr('data-row-id');
            row.find('button').attr('disabled','disabled');
            $.ajax({
                url: '{{ url("api/labels/") }}/'+dbrowid,
                type: 'DELETE',
                success: function(){
                    removeRow(rowid);           
                },
                error: function(){
                    alert('Opss something wrong');
                    row.find('button').removeAttr('disabled');
                }
            })
        }

        function editRow(rowid){
            var row = $(rowid);
            row.find(".row-col input").show();
            row.find(".text-col").hide();
            row.find('.submit-btn').show();
            row.find('.cancel-btn').show();
            row.find('.edit-btn').hide();
            row.find('.delete-btn').hide();
        }

        function cancelEditRow(rowid){
            var row = $(rowid);
            row.find(".row-col input").hide();
            row.find(".text-col").show();
            row.find('.submit-btn').hide();
            row.find('.cancel-btn').hide();
            row.find('.edit-btn').show();
            row.find('.delete-btn').show();
        }

        function removeRow(rowid){
            $(rowid).remove();
        }
        
        function updateRow(rowid,data){
            var row = $(rowid);

            row.html(
                '<td class="row-col">'+
                    '<span class="text-col">'+data.name+'</span>'+
                    '<input type="text" name="name" class="input-col form-control" style="display:none;" value="'+data.name+'">'+
                '</td>'+
                '<td class="action-col">'+
                    '<button type="button" onclick="saveRow(\'#label-'+data.id+'\',\'edit\')" class="btn btn-outline-success btn-sm submit-btn" style="display:none;"><i class="fa fa-check"></i></button> '+
                    '<button type="button" onclick="editRow(\'#label-'+data.id+'\')" class="btn btn-outline-primary btn-sm edit-btn"><i class="fa fa-pencil"></i></button> '+
                    '<button type="button" onclick="deleteRow(\'#label-'+data.id+'\')" class="btn btn-outline-danger btn-sm delete-btn"><i class="fa fa-trash"></i></button> '+
                    '<button type="button" onclick="cancelEditRow(\'#label-'+data.id+'\')" class="btn btn-outline-danger btn-sm cancel-btn" style="display:none;"><i class="fa fa-times"></i></button> '+
                '</td>'
            );
        }

        function addRow(tableid,data){
            var table = $(tableid);
            var tableBody = table.find('tbody');

            tableBody.append(
                '<tr id="label-'+data.id+'" data-row-id="'+data.id+'">'+
                    '<td class="row-col">'+
                        '<span class="text-col">'+data.name+'</span>'+
                        '<input type="text" name="name" class="input-col form-control" style="display:none;" value="'+data.name+'">'+
                    '</td>'+
                    '<td class="action-col">'+
                        '<button type="button" onclick="saveRow(\'#label-{{ $label->id }}\',\'edit\')" class="btn btn-outline-success btn-sm submit-btn" style="display:none;"><i class="fa fa-check"></i></button> '+
                        '<button type="button" onclick="editRow(\'#label-'+data.id+'\')" class="btn btn-outline-primary btn-sm edit-btn"><i class="fa fa-pencil"></i></button> '+
                        '<button type="button" onclick="deleteRow(\'#label-'+data.id+'\')" class="btn btn-outline-danger btn-sm delete-btn"><i class="fa fa-trash"></i></button> '+
                        '<button type="button" onclick="cancelEditRow(\'#label-'+data.id+'\')" class="btn btn-outline-danger btn-sm cancel-btn" style="display:none;"><i class="fa fa-times"></i></button> '+
                    '</td>'+
                '</tr>'
            );
        }

        function saveRow(rowid,state){
            var row = $(rowid);
            var rowData = {};
            var type = "POST";
            var url = '{{ route("labels.store") }}';
            if (state == 'edit') {
                type = "PUT";
                url = '{{ url("api/labels") }}/'+row.attr('data-row-id');
            } 
            row.find('.row-col').each(function(i,el){
                input = $(el).find('input');
                rowData[input.attr('name')] = input.val()  
            });

            row.find('input').attr('disabled','disabled');
            row.find('.action-col button')
                .attr('disabled','disabled');
            row.find(".submit-btn > i").removeClass()
                .addClass("fa fa-spinner fa-spin");
            $.ajax({
                url: url,
                type: type,
                data: rowData,
                success: function(response){
                    if (state == "insert") {
                        removeRow(rowid);
                        addRow('#labels-table',response.data);
                    }else if(state == "edit"){
                        updateRow(rowid,response.data);
                    }
                },
                error: function(err){
                    alert('Opss something wrong');
                    row.find('input').removeAttr('disabled');
                    row.find('.action-col button')
                        .removeAttr('disabled');
                    row.find(".submit-btn > i").removeClass()
                        .addClass("fa fa-check");
                }
            })
        }
    </script>
@endsection