<?php

namespace App\Http\Controllers\Api;

use App\Labels;
use Illuminate\Http\Request;
use App\Http\Resources\LabelResource;
use App\Http\Controllers\Controller;

class LabelsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return LabelResource::collection(Labels::paginate());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return new LabelResource(Labels::create($request->all()));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $label = Labels::find($id);
        $label->update($request->all());
        return new LabelResource($label);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Labels::destroy($id)){
            return response()->json(['status'=>'success'],200);
        }else{
            return response()->json(['status'=>'error'],500);
        }
    }
}
