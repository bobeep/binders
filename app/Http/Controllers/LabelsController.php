<?php

namespace App\Http\Controllers;

use App\Labels;
use Illuminate\Http\Request;

class LabelsController extends Controller
{

    public function __construct()
    {
        $this->passed_data = collect([]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page_title = "Labels"; 
        $labels = Labels::paginate();
        return view('labels.index',compact('page_title','labels'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Labels  $labels
     * @return \Illuminate\Http\Response
     */
    public function show(Labels $labels)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Labels  $labels
     * @return \Illuminate\Http\Response
     */
    public function edit(Labels $labels)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Labels  $labels
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Labels $labels)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Labels  $labels
     * @return \Illuminate\Http\Response
     */
    public function destroy(Labels $labels)
    {
        //
    }
}
