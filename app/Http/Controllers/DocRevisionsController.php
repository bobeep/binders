<?php

namespace App\Http\Controllers;

use App\DocRevisions;
use Illuminate\Http\Request;

class DocRevisionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DocRevisions  $docRevisions
     * @return \Illuminate\Http\Response
     */
    public function show(DocRevisions $docRevisions)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DocRevisions  $docRevisions
     * @return \Illuminate\Http\Response
     */
    public function edit(DocRevisions $docRevisions)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DocRevisions  $docRevisions
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DocRevisions $docRevisions)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DocRevisions  $docRevisions
     * @return \Illuminate\Http\Response
     */
    public function destroy(DocRevisions $docRevisions)
    {
        //
    }
}
