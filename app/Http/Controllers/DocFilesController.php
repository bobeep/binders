<?php

namespace App\Http\Controllers;

use App\DocFiles;
use Illuminate\Http\Request;

class DocFilesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DocFiles  $docFiles
     * @return \Illuminate\Http\Response
     */
    public function show(DocFiles $docFiles)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DocFiles  $docFiles
     * @return \Illuminate\Http\Response
     */
    public function edit(DocFiles $docFiles)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DocFiles  $docFiles
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DocFiles $docFiles)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DocFiles  $docFiles
     * @return \Illuminate\Http\Response
     */
    public function destroy(DocFiles $docFiles)
    {
        //
    }
}
