<?php
/**
 * Scenery Component for viewing more cleaner
 */


namespace App\Components;
use Route;

trait Scenery
{

    private $passed_data;

    /**
     * Scenery render view, default view take from route name
     * @return view
     */
    private function renderView(array $data = [])
    {
       $data = $this->passed_data->merge($data);
       $view =  $data->has('view') ? $data->get('view') : Route::currentRouteName();
       return view($view, $this->getPassedDatas()); 
    }

    /**
     * Get passed data for view except view data
     * @return array
     */

    private function getPassedDatas() : array
    {
        return $this->passed_data->except('view')->toArray();
    }

    /**
     * Set passed data to view with array parameters
     * @return void
     */
    private function setData(array $data)
    {
        $this->passed_data =  $this->passed_data->merge($data);
    }


}
